# Curso de NGINX Desde Cero - CodeStack

En este tutorial conoceremos un poco sobre NGINX, sobre sus funciones como servidor HTTP, Proxy y de Balanceo de Carga. 

https://www.youtube.com/watch?v=eyxpLa9hUS8&list=PLYAyQauAPx8kwPdi9v1d_FGdJ50_li_WE

En este tutorial conoceremos un poco sobre NGINX, sobre sus funciones como servidor HTTP, Proxy y de Balanceo de Carga. 
Conoceremos como instalarlo en Debian 8, CentOS 7, OpenSuse Tumbleweed/42, Fedora 23 y Windows.

##- Debian y derivados *probado en debian 8 y LinuxMint 17/18*
Repositorio de PHP 7 para Ubuntu/LinuxMint
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php7.0

##Repositorio de PHP 7 para Debian 8
deb http://packages.dotdeb.org jessie all
deb-src http://packages.dotdeb.org jessie all
Llave GPG
wget https://www.dotdeb.org/dotdeb.gpg
sudo apt-key add dotdeb.gpg

apt-get install nginx php7.0 php7.0-fpm
apt-get install php7.0-mysql mariadb-server mariadb-client

##- OpenSuse *probado en Tumbleweed y 42.2*
zypper install php7 php7-fpm nginx
zypper install mariadb mariadb-client php7-mysql

##- CentOS *probado en CentOS 7*
yum install epel-release
yum install nginx
rpm -Uvh https://dl.fedoraproject.org/pub/epel...
rpm -Uvh https://mirror.webtatic.com/yum/el7/w...
yum install php70w php70w-opcache php70w-fpm
yum install mariadb mariadb-server

##- Fedora *probado en Fedora 23*
dnf install nginx
wget http://rpms.remirepo.net/fedora/remi-...
dnf install remi-release-23.rpm
dnf config-manager --set-enabled remi-php70
yum update
dnf install mariadb-server mariadb-client

##- Windows *Probado en Windows 8, 8.1 y 10*
Descargar el zip y descomprimir, se recomienda descomprimir en la raiz del sistema.
http://nginx.org/en/download.html
Para instalar MariaDB y PHP en Windows sigue las instrucciones de este video:
https://www.youtube.com/watch?v=Unx_Q...

Para que se pueda tener acceso desde otro equipo *en el que caso de que en GNU/Linux no lo permita iptables* son necesarias las siguientes reglas:
iptables -F  (OJO: Este comando borra todas las reglas de iptables existentes)
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 22 -j ACCEPT (para aceptar conexion via ssh)
iptables -A INPUT -j DROP
iptables -A OUTPUT -j DROP
-------------------------------------------------------
